module github.com/iamstefin/crumbchain

go 1.14

require (
	github.com/cheggaaa/pb/v3 v3.0.5
	github.com/golang/protobuf v1.4.1
	google.golang.org/protobuf v1.25.0
)
